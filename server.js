const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

const { ExpressPeerServer } = require('peer')

const peerServer = ExpressPeerServer(server, {
	debug: true,
})

const { v4: uuidv4 } = require('uuid')

app.use('/peerjs', peerServer)
app.use(express.static('public'))
app.set('view engine', 'ejs')

app.get('/', (req, res) => {
	res.redirect(`/${uuidv4()}`)
})

app.get('/:room/:name/:permission', (req, res) => {
	console.log("hello");
	res.render('room', { roomId: req.params.room,name: req.params.name,permission: req.params.permission })
	
})

// app.get ('/home/1', (req,res)=>{
// 	res.render( 'home' )
// })
// user ={}; 


io.on('connection', (socket) => {
	socket.on('join-room', (roomId, userId,userName,userType) => {
		console.log(userName)
		console.log(userType)

		socket.join(roomId)
		socket.to(roomId).broadcast.emit('user-connected', userId)

		socket.on('message', (message) => {
			io.to(roomId).emit('createMessage', message, userName)
		})
		socket.on('disconnect', () => {
			console.log("ajj")
			socket.to(roomId).broadcast.emit('user-disconnected', userId);
		    //  delete user[socket.userId];
		})
	})
})

const PORT = process.env.PORT || 5000


server.listen(PORT, () => console.log(`Listening on port ${PORT}`))
 